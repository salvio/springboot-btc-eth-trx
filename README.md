# 重要提示：<br/>本文涉及的内容，只用来学习，不对投资做成任何建议
# springboot-btc-eth-trx

#### 介绍
使用springboot+web3j方式进行对ETH链，BTC链，币安链，波场链等热门区块链进行链上开发，开箱即可用，大大缩小了入门门槛。
如有帮助请记得 **点击右上角star** 。

```
1.项目才刚刚开始，后面还有很多丰富的功能
2.请关注我们了解后续的情况（不定期更新）
```
**下一个准备开源项目**

1. 亲，动动您发财的手为我们点一颗star，是对我们最好的鼓励和支持，也是我们前进的动力<br/>
2. 演示环境<br/>

　　　　[铭文诊断管理系统：http://insc.wechatqun.cn:8088](http://insc.wechatqun.cn:8088)

**智能AI已开源项目**
1. 演示环境<br/>

　　　　[超级AI大脑-前端：http://www.mj.ink](http://www.mj.ink)

　　　　[超级AI大脑-后端：http://gpt.ht.mj.ink:8899](http://gpt.ht.mj.ink:8899)

　　　　[超级AI大脑-技术社区 http://doc.mj.ink](http://doc.mj.ink)

## 项目关系

| 项目                                                              | Star                                                                                                                                                                                                                                                                                             | 简介                          |
|-----------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------|
| [超级AI大脑](https://gitee.com/ylzl/springboot-openai-chatgpt)  | [![Gitee star](https://gitee.com/ylzl/springboot-openai-chatgpt/badge/star.svg)](https://gitee.com/ylzl/springboot-openai-chatgpt)       | 基于 Spring Boot 单体架构的ChatGpt        |
| [铭文诊断管理系统](https://gitee.com/Linriqiang/springboot-insc)  | [![Gitee star](https://gitee.com/Linriqiang/springboot-insc/badge/star.svg)](https://gitee.com/Linriqiang/springboot-insc)       | 对Brc20 Ordinals的铭文数据进行了诊断分析      |

## 合作（广告勿扰）：

 <div align=center >
    <td ><img height="350" width="250" src="https://ai.oss.mj.ink/chatgpt/insc/wx.jpg"/></td>
 </div>


